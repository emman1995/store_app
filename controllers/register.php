<?php
class Register extends Controller{

  function __construct(){
    parent::__construct();
    // Session::init();
    // $loggedIn = Session::get("loggedIn");
    // if($loggedIn == false){
    //   Session::destroy();
    //   echo("<script>location.href = 'http://localhost/store_app/login';</script>");
    //   // header('location: ./login');
    //   exit;
    // }
  }

  function index(){
    $this->view->render('register/index');
  }


  function registerUser(){
      if($_POST){

      $fname = strip_tags($_POST['fname']);
      $lname = strip_tags($_POST['lname']);
      $email = strip_tags($_POST['email']);
      $username = strip_tags($_POST['username']);
      $password = strip_tags($_POST['password']);

      $data = array();
      $data['fname'] = $fname;
      $data['lname'] = $lname;
      $data['email'] = $email;
      $data['username'] = $username;
      $data['password'] = $password;

      $this->model->registerNewUser($data);

    }
  }

}

