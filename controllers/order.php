<?php
class Order extends Controller{

  function __construct(){
    parent::__construct();
    Session::init();
    $loggedIn = Session::get("loggedIn");
    if($loggedIn == false){
      Session::destroy();
      echo("<script>location.href = 'http://localhost/store_app/login';</script>");
      // header('location: ./login');
      exit;
    }
  }

  function index(){
     $this->view->getOrders = $this->model->getOrders();

    $this->view->render('order/index');
  }

  function addOrders(){
      if($_POST){

      $ordername = strip_tags($_POST['ordername']);
      $countname = strip_tags($_POST['countname']);
     

      $data = array();
      $data['ordername'] = $ordername;
      $data['countname'] = $countname;
      
      $this->model->addNewOrder($data);

    }
  }

}

