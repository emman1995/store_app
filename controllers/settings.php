<?php
class Settings extends Controller{

  function __construct(){
    parent::__construct();
    Session::init();
    $loggedIn = Session::get("loggedIn");
    if($loggedIn == false){
      Session::destroy();
      echo("<script>location.href = 'http://localhost/store_app/login';</script>");
      // header('location: ./login');
      exit;
    }
  }

  function index(){
    $this->view->render('settings/index');
  }
}

